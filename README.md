# SRB2Kart (Snowy Build)

[SRB2Kart](https://srb2.org/mods/) is a kart racing mod based on the 3D Sonic the Hedgehog fangame [Sonic Robo Blast 2](https://srb2.org/), based on a modified version of [Doom Legacy](http://doomlegacy.sourceforge.net/).

## What's new?
- A (lot?) of changes actually! But im too lazy to list them here...
- I don't know, you can now open the console everywhere now!

## Disclaimer
- Kart Krew is in no way affiliated with SEGA or Sonic Team. We do not claim ownership of any of SEGA's intellectual property used in SRB2.
- This build is based on Vanilla Kart, neither Moe-Mansion nor Birdhouse.

- This custom build uses a custom file, which can be downloaded right here:
	- [snowy_files.kart](https://cdn.discordapp.com/attachments/977366683201187890/1018649762926444705/snowy_files.kart)
- Keep in mind that this can be changed sometimes.